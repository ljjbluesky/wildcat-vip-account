# 野猫VIP帐号 - WildcatVipAccount
![image](https://img.shields.io/badge/build-passing-brightgreen)
![image](https://img.shields.io/badge/license-MIT-blue)
![image](https://img.shields.io/badge/stars-%E2%98%85%E2%98%85%E2%98%85%E2%98%85%E2%98%85-brightgreen)
## 秒记野猫短网址，从此不迷路：[git.io/yemao](https://git.io/yemao) 或者 [git.io/52vip](https://git.io/52vip)

## 油猴Tampermonkey/暴力猴Violentmonkey版本（适用于百度PC网页）：
https://greasyfork.org/zh-CN/scripts/390890-svip-%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98%E4%BC%9A%E5%91%98-%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98vip-%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98svip%E5%85%8D%E8%B4%B9%E8%8E%B7%E5%8F%96-%E4%BB%A3%E6%9B%BFidm-xdown-pandownload%E7%BD%91%E9%A1%B5%E7%89%88-%E9%95%BF%E6%9C%9F%E6%9B%B4%E6%96%B0-%E6%94%BE%E5%BF%83%E4%BD%BF%E7%94%A8

## 微信小程序版本（用小程序版本更便捷，记得收藏到“我的小程序”AND“添加到桌面”，墙裂推荐！）：
### `火雕520（理解成“火雕我爱你”，1秒记住！！）`
![image](https://raw.githubusercontent.com/wuxingsanren/wildcat-vip-account/master/images/wxapp_qrcode_huodiao.png)

## 反馈联系：
wuxingsanren2008#gmail.com（#改为@）

## License
MIT

Copyright (c) 2019 五行散人

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
